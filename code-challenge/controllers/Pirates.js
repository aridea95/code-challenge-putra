const { Pirate } = require ('../models')

class PiratesController {
    static getPirate(req,res){
        Pirate.findAll()
        .then(result=>{
            //res.send(result)
            res.render('pirate.ejs', {pirates: result})
        })
        .catch(err=>{
            console.log(err);
        })
    }
    static addformPirate(req, res){
        res.render('addPirate.ejs');
    }
    static addPirate(req, res){
        const { name, status, haki, ability } = req.body;
        Pirate.create({
            name,
            status, 
            haki,
            ability
        })
            .then(result=>{
                //res.send(result)
                res.redirect('/pirates')
            })
            .catch(err=>{
                res.send(err)
            })
    }
    static deletePirate(req, res){
        const id = req.params.id;
        Pirate.destroy({
            where: { id }
        })
            .then(()=>{
                //res.send("Deleted")
                res.redirect('/pirates')
            })
            .catch(err => {
                res.send(err)
            })
    }

}

module.exports = PiratesController;