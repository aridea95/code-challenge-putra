const { Ship } = require ('../models')

class ShipsController {
    static getShips(req,res){
        Ship.findAll()
        .then(result=>{
            //res.send(result)
            res.render('ships.ejs', {ships: result})
        })
        .catch(err=>{
            console.log(err);
        })
    }
    static addformShips(req, res){
        res.render('addShip.ejs');
    }
    static addShip(req, res){
        const { name, type, power, ability } = req.body;
        Ship.create({
            name,
            type, 
            power,
            ability
        })
            .then(result=>{
                //res.send(result)
                res.redirect('/ships')
            })
            .catch(err=>{
                res.send(err)
            })
    }
    static deleteShip(req, res){
        const id = req.params.id;
        Ship.destroy({
            where: { id }
        })
            .then(()=>{
                //res.send("Deleted")
                res.redirect('/ships')
            })
            .catch(err => {
                res.send(err)
            })
    }

}

module.exports = ShipsController;