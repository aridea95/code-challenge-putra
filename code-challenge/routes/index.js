const { Router } = require ('express');
const router = Router ();
const shipsRoutes = require ('./ships')
const piratesRoutes = require ('./pirates')

router.get('/', (req,res)=>{
    res.render('index.ejs')
});
router.get('/ships', (req,res)=>{
    res.render('ships.ejs')
});
router.get('/pirates', (req, res)=>{
    res.render('pirate.ejs')
})
router.use('/ships', shipsRoutes)
router.use('/pirates', piratesRoutes)

module.exports = router;