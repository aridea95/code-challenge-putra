const { Router } =  require ('express');
const router = Router();
const PiratesController = require('../controllers/Pirates')

// router.get('/', PiratesController.getPirate)
router.get('/pirates', PiratesController.getPirate)
router.get('/pirates/add', PiratesController.addformPirate)
router.post('/pirates/add', PiratesController.addPirate)
router.get('/pirates/delete/:id', PiratesController.deletePirate)

module.exports = router;