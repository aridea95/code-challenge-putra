const { Router } =  require ('express');
const router = Router();
const ShipsController = require('../controllers/Ships')

// router.get('/', ShipsController.getShips)
router.get('/ships', ShipsController.getShips)
router.get('/ships/add', ShipsController.addformShips)
router.post('/ships/add', ShipsController.addShip)
router.get('/ships/delete/:id', ShipsController.deleteShip)

module.exports = router;